package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping
    @CrossOrigin("*")
    public ResponseEntity<String> checkAppRunning() {
        return ResponseEntity.ok("Hello");
    }
}
